package com.shymoniak.view;

public enum  ConsoleEnum {
    SPARATION_LINE("------------------------------------") ,
    ALL_OPERATIONS(" Choose task: "),
    FIRST_OPERATION("\t\t1. Task 1"),
    SECOND_OPERATION("\t\t2. Task 2"),
    THIRD_OPERATION("\t\t3. Task 3"),
    FOURTH_OPERATION("\t\t3. Task 4"),
    NINETH_OPERATION("\t\t9. Exit");

    private final String text;

    ConsoleEnum(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
