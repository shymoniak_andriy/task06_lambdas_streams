package com.shymoniak.view;

import com.shymoniak.model.MainModel;

import com.shymoniak.model.task2.CommandsEnum;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Scanner;

public class View {
    private static final Logger LOGGER = LogManager.getLogger(View.class);
    MainModel mainModel = new MainModel();
    Scanner scan = new Scanner(System.in);

    public void startApp() {

        int fromUser = getUserChoice();

        while (fromUser != 9) {
            if (fromUser == 1) {
                LOGGER.trace("Enter three numbers");
                int a = scan.nextInt();
                int b = scan.nextInt();
                int c = scan.nextInt();
                mainModel.printTask1(a, b, c);
            } else if (fromUser == 2) {
                String commandName = getCommandName();
                if (isRightCommand(commandName)){
                    LOGGER.trace("Enter any string as a parameter");
                    String parameter = scan.next();
                    mainModel.printTask2(commandName, parameter);
                } else {
                    LOGGER.error("You entered wrong command name");
                }
            } else if (fromUser == 3) {
                LOGGER.trace("Enter length of list and it's max number");
                mainModel.printTask3(scan.nextInt(), scan.nextInt());
            } else if (fromUser == 4){
                LOGGER.trace("Enter any text (press enter if you finished)");
                mainModel.printTask4();
            }
            fromUser = getUserChoice();
        }
    }

    private int getUserChoice() {
        LOGGER.info("\n" + ConsoleEnum.ALL_OPERATIONS +
                "\n" + ConsoleEnum.FIRST_OPERATION.toString() +
                "\n" + ConsoleEnum.SECOND_OPERATION.toString() +
                "\n" + ConsoleEnum.THIRD_OPERATION +
                "\n" + ConsoleEnum.FOURTH_OPERATION +
                "\n" + ConsoleEnum.NINETH_OPERATION);
        return scan.nextInt();
    }

    private String getCommandName(){
        LOGGER.info( "\nEnter one of following commands in console:"
                + "\n" + CommandsEnum.AS_LAMBDA
                + "\n" + CommandsEnum.AS_METHOD_REFERENCE
                + "\n" + CommandsEnum.AS_ANONYMOUS_CLASS
                + "\n" + CommandsEnum.AS_OBJECT_OF_CLASS
        );
        return scan.next();
    }

    private boolean isRightCommand(String commandName){
        return CommandsEnum.AS_LAMBDA.toString().contains(commandName)
                || CommandsEnum.AS_METHOD_REFERENCE.toString().contains(commandName)
                || CommandsEnum.AS_ANONYMOUS_CLASS.toString().contains(commandName)
                || CommandsEnum.AS_OBJECT_OF_CLASS.toString().contains(commandName);
    }
}