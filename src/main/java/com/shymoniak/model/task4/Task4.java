package com.shymoniak.model.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Create application. User enters some number of text lines (stop
 * reading text when user enters empty line). Application returns:
 * - Number of unique words
 * - Sorted list of all unique words
 * - Word count. Occurrence number of each word in the text
 * (e.g. text “a s a” -> a-2 s-1 ). Use grouping collector.
 * - Occurrence number of each symbol except upper case
 * characters
 */
public class Task4 {
    private static final Logger LOGGER = LogManager.getLogger(Task4.class);

    public List<String> getUserText() {
        Scanner scan = new Scanner(System.in);
        List<String> list = new ArrayList<>();
        String fromUser;

        do {
            fromUser = scan.nextLine();
            if (fromUser.equals("")) {
                break;
            }
            list.add(fromUser);
        } while (true);

        list = list.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.toList());

        return list;
    }

    public void printList(List<String> list) {
        list.stream()
                .forEach(s -> System.out.print(s + " "));
    }

    public long numberOfUnique(List<String> list) {
        return list.stream()
                .distinct()
                .count();
    }

    public List<String> sortedUniqueList(List<String> list) {
        return list.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public Map<String, Long> wordCounter(List<String> list) {
        return list.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    public Map<String, Long> lowerCaseSymbolAccurance(List<String> list) {
        return list.stream()
                .filter(s -> s != s.toUpperCase())
                .flatMap(e -> Stream.of(e.split("")))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

}
