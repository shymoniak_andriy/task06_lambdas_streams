package com.shymoniak.model.task3;

import org.apache.logging.log4j.LogManager;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Create a few methods that returns list (or array) of random
 * integers. Methods should use streams API and should be
 * implemented using different Streams generators.
 * <p>
 * Count average, min, max, sum of list values. Try to count
 * sum using both reduce and sum Stream methods
 * <p>
 * Count number of values that are bigger than average
 */
public class Task3 {

    private static final Random RANDOM = new Random();

    public String mathOperations(List<Integer> list) {

        printList(list);
        //Three ways to count max value in a list
        int max = list.stream()
                        .mapToInt(v -> v)
                        .max()
                        .orElseThrow(NoSuchElementException::new);

        int max2 = list.stream()
                        .reduce(0, Integer::max);

        int max3 = list.stream()
                        .reduce(0, (a, b) -> a > b ? a : b);

        //The same operations used to find max element can be used here
        int min = list.stream()
                        .mapToInt(v -> v)
                        .min()
                        .orElseThrow(NoSuchElementException::new);

        //Two ways to count sum of all elements in the list
        int sum = list.stream()
                        .reduce(0, Integer::sum);

        int sum2 = list.stream()
                        .mapToInt(Integer::intValue).sum();

        double average = list.stream()
                              .mapToInt(v->v)
                              .average()
                              .orElseThrow(NoSuchElementException::new);;

        long biggerThanAverage = list.stream()
                                        .filter(s -> s > average).count();

        return "\nMax element in list: " + max + " (checking: " + max2 + ", " + max3 + ")"
                + "\nmin element: " + min
                + "\nsum of all elements: " + sum + "(checking: " + sum2 + ")"
                + "\naverage value: " + average
                + "\nbigger than average (" + average + "): " + biggerThanAverage;
    }

    public void printList(List<Integer> list) {
        list.stream().forEach(s -> LogManager.getLogger(Task3.class).trace(s + ", "));
    }

    /**
     * Creating stream using method - iterate
     *
     * @param length size of List
     * @param maxNum the biggest number in sequence
     * @return List filled wih random numbers
     */
    public List<Integer> generateList1(int length, int maxNum) {
        Stream<Integer> stream = Stream.iterate(0, s -> Math.abs(RANDOM.nextInt() % maxNum))
                                        .limit(length);
        return stream.collect(Collectors.toList());
    }

    /**
     * Creating stream using arrays
     *
     * @param length size of List
     * @param maxNum the biggest number in sequence
     * @return List filled wih random numbers
     */
    public List<Integer> generateList2(int length, int maxNum) {
        Integer[] arr = new Integer[length];
        for (int i = 0; i < length; i++) {
            arr[i] = Math.abs(RANDOM.nextInt() % maxNum);
        }
        Stream<Integer> stream = Stream.of(arr);
        return stream.collect(Collectors.toList());
    }

    /**
     * Creating stream using method - generate
     *
     * @param length size of List
     * @param maxNum the biggest number in sequence
     * @return List filled wih random numbers
     */
    public List<Integer> generateList3(int length, int maxNum) {
        Stream<Integer> stream = Stream.generate(() -> Math.abs(RANDOM.nextInt() % maxNum))
                                        .limit(length);
        return stream.collect(Collectors.toList());
    }
}
