package com.shymoniak.model.task2;

public enum CommandsEnum {

    AS_LAMBDA("\tlambda function"),
    AS_METHOD_REFERENCE("\tmethod reference"),
    AS_ANONYMOUS_CLASS("\tanonymous class"),
    AS_OBJECT_OF_CLASS("\tobject of command class");

    private final String text;

    CommandsEnum(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
