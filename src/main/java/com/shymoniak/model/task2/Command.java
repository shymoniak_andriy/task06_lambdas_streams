package com.shymoniak.model.task2;

@FunctionalInterface
interface Command {
    String invoke(String str);
}