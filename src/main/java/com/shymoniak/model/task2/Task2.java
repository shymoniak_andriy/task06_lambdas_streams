package com.shymoniak.model.task2;

import com.shymoniak.model.MainModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implement pattern Command. Each command has its name
 * (with which it is invoked) and one string argument. You should
 * implement 4 commands with next ways: command as lambda
 * function, as method reference, as anonymous class, as object of
 * command class. User enters command name and argument into
 * console, your app invokes corresponding command.
 */
public class Task2 implements Command {

    private static final Logger LOGGER = LogManager.getLogger(MainModel.class);

    public void lambdaFunc(String str) {
        Command lambda = str1 -> "Lambda " + str1;
        LOGGER.trace(lambda.invoke(str));
    }

    public void methodReference(String str) {
        Command command = new Task2()::printMethodReference;
        LOGGER.trace(command.invoke(str));
    }

    public String printMethodReference(String str) {
        return "Method referance " + str;
    }

    public void anonymousClass(String str) {
        Command command = new Command() {
            @Override
            public String invoke(String str) {
                return "Anonymous class " + str;
            }
        };
        LOGGER.trace(command.invoke(str));
    }

    @Override
    public String invoke(String str) {
        return "Common class " + str;
    }

}
