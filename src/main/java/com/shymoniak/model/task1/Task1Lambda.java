package com.shymoniak.model.task1;

@FunctionalInterface
public interface Task1Lambda{
    int func(int a, int b, int c);
}