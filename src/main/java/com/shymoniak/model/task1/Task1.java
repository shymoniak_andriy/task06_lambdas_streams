package com.shymoniak.model.task1;

/**
 * Create functional interface with method that accepts three int
 * values and return int value. Create lambda functions (as
 * variables in main method) what implements this interface:
 * - First lambda returns max value
 * - Second – average
 * Invoke thous lambdas.
 */
public class Task1 {

    public int findMax(int num1, int num2, int num3){
        Task1Lambda lambda = (a, b, c) -> Math.max(Math.max(a, b), c);
        return lambda.func(num1, num2, num3);
    }

    public double average(int num1, int num2, int num3){
        Task1Lambda lambda = (a, b, c) -> (a + b + c)/3;
        return lambda.func(num1, num2, num3);
    }
}
