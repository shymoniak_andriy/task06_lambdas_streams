package com.shymoniak.model;

import com.shymoniak.model.task1.Task1;
import com.shymoniak.model.task2.CommandsEnum;
import com.shymoniak.model.task2.Task2;
import com.shymoniak.model.task3.Task3;
import com.shymoniak.model.task4.Task4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class MainModel {
    private Task1 t1 = new Task1();
    private Task2 t2 = new Task2();
    private Task3 t3 = new Task3();
    private Task4 t4 = new Task4();
    private static final Logger LOGGER = LogManager.getLogger(MainModel.class);

    public void printTask1(int a, int b, int c) {
        LOGGER.trace("Max number of " + a + ", " + b + ", " + c +
                " = " + t1.findMax(a, b, c));
        LOGGER.trace("Average value of three = " + t1.average(a, b, c));
    }

    public void printTask2(String commandName, String parameter) {
        if (CommandsEnum.AS_ANONYMOUS_CLASS.toString().contains(commandName)) {
            t2.anonymousClass(parameter);
        } else if (CommandsEnum.AS_LAMBDA.toString().contains(commandName)) {
            t2.lambdaFunc(parameter);
        } else if (CommandsEnum.AS_OBJECT_OF_CLASS.toString().contains(commandName)) {
            LOGGER.trace(t2.invoke(parameter));
        } else if (CommandsEnum.AS_METHOD_REFERENCE.toString().contains(commandName)) {
            t2.methodReference(parameter);
        }
    }

    public void printTask3(int length, int maxNum){
        LOGGER.trace(t3.mathOperations(t3.generateList1(length, maxNum)));
    }

    public void printTask4(){
        List<String> list = t4.getUserText();
        LOGGER.trace("User entered following text:\n");
        t4.printList(list);
        long numberOfUnique = t4.numberOfUnique(list);
        LOGGER.trace("There are " + numberOfUnique + " unique words in the text\n");
        List<String> sortedUnique = t4.sortedUniqueList(list);
        LOGGER.trace("Sorted list of unique words looks as follows:\n");
        t4.printList(sortedUnique);
        Map<String,Long> wordCount = t4.wordCounter(list);
        LOGGER.trace("Words occurrence:\n");
        wordCount.forEach((key, value) -> System.out.print(key + ":" + value + "\t"));
        Map<String,Long> lcsa = t4.lowerCaseSymbolAccurance(list);
        LOGGER.trace("Lower case symbols occurrence:\n");
        lcsa.forEach((key, value) -> System.out.println(key + ":" + value + "\t"));
    }
}
